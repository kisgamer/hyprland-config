## Hyprland config

# Requirements
- hyprpaper
- copyq
- waybar
- dunst
- nm-applet
- qt5ct/setqttheme
- discord
- pa-notify
- JetBrains Mono font family

# Installation
1. Install hyprland
2. Install requirements
3. copy the ```hypr``` folder to ```~/.config/```
4. I think that's it.
